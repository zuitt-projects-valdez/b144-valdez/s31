//responsible for defining the urls

const express = require("express");
const taskController = require("../controllers/taskController"); //to be able to the getAllTasks function
//creates a router instance that functions as a middleware and routing system
//allows access to http method middlewares that makes in easier to create routes for our application
const router = express.Router(); //middleware router; allows us to have access to http methods

//route for get all tasks
//base url: http://localhost:3001/tasks
//get url: http://localhost:3001/tasks/
router.get("/", (req, res) => {
  taskController
    .getAllTasks()
    .then((resultFromController) => res.send(resultFromController)); //the result from the get all tasks will go to the then arrow function and send the results
});

//create a route for creating a task
router.post("/", (req, res) => {
  taskController.createTask(req.body).then((result) => res.send(result));
});

//route for deleting a task
//:id - parameter of the supposed exact value = called wildcard (if there is a : before the id)
//http://localhost:3001/tasks/:id
//the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//the word that comes after the colon will be the name of the url parameter
//:id is called WILDCARD where you can put any value which then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (req, res) => {
  //url parameter values are accessed via the rquest object's "params" property
  //property name of this object will match the given url parameter name
  //in this case "id" is the name of the parameter
  taskController.deleteTask(req.params.id).then((result) => res.send(result));
});

//create a route for updating a task
router.put("/:id", (req, res) => {
  taskController
    .updateTask(req.params.id, req.body)
    .then((result) => res.send(result));
});

// Activity:
//route for a specific task
router.get("/:id", (req, res) => {
  taskController.getOneTask(req.params.id).then((result) => res.send(result));
});

//route for updating the status
router.patch("/:id/complete", (req, res) => {
  taskController
    .updateStatus(req.params.id, req.body)
    .then((result) => res.send(result));
});

module.exports = router;
