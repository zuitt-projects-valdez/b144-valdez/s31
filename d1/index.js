//set up the dependencies
const express = require("express");
const mongoose = require("mongoose");
//this allows us to use all the routes defined in 'taskRoute.js
const taskRoute = require("./routes/taskRoute");
//server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//database connection
mongoose.connect(
  "mongodb+srv://vvaldez626:admin@wdc028-course-booking.0r6u7.mongodb.net/batch144-to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console.error, "connection error"));
db.once("open", () => console.log(`We're connected to the cloud database`));

//routes(base URI for task route)
//allows all the task routes created in the taskRoute.js file to use '/tasks' route
app.use("/tasks", taskRoute);
//http://localhost:3001/tasks -

//listen
app.listen(port, () => console.log(`Now listening to port ${port}`));
