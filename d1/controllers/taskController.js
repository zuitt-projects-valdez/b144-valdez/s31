//controllers contain the functions and business logic of our express js app
// all the operations will be placed in this file
//use require so we can access the models in this file
const Task = require("../models/task");

//controller function for getting all the tasks
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

//controller function for creating a task
module.exports.createTask = (requestBody) => {
  //create a task object based on the mongoose model 'task'
  let newTask = new Task({
    name: requestBody.name,
  });
  //save - add a return statement because using .then and can't use it without return; .then waits for the statement before to be completed (promise fulfilled)
  //.then method accepts two parameters: save, error respectively
  // first: stores the result returned by the mongoose save method
  //the second parameter will store the error object
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false; //prevents any further code to be executed if an error occurs ; should have a return statement with the .then method
      //since the following return statement is nested within the then method chained to the save method, they do not prevent each other from executing code
      // the else statement will no longer be evaluated
    } else {
      //if save successful return the new task object
      return task;
    }
  });
};

//controller function for deleting a task
/* 
  Business logic
  1. look for the task with the corresponding id provided in the URL/route 
  2. delete the task 
*/

module.exports.deleteTask = (taskId) => {
  //findByIdAndRemove mongoose method will look for a task with the same id provided from the url and remove/delete the document from mongo db; looks for the document using the "_id" field
  return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
    if (err) {
      console.log(error);
      return false;
    } else {
      return removedTask;
    }
  });
};

//controller function for updating a task
/* 
  Business logic 
  1. get the task with the id 
  2. replace the task's name returned from the database with the "name" property from the reqeust body 
  3. save the task  
*/

module.exports.updateTask = (taskID, newContent) => {
  //instead of find({"_id": "value"})
  return Task.findById(taskID).then((result, error) => {
    //if an error is encountered, return a false
    if (error) {
      console.log(error);
      return false;
    }
    //results of the "findById" method will be stored in the "result" parameter
    //it's "name" property will be reassigned the value of the "name" received from the request
    result.name = newContent.name;
    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return false;
      } else {
        return updatedTask;
      }
    });
  });
};

//Activity:
/* 
  Business logic: controller function for retrieiving a specific task
  1. find match/ find task by id 
  2. display the task 
*/

module.exports.getOneTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return result;
    }
  });
};

/* 
  Business logic: controller function for updating the status 
  1. find by id 
  2. replace status with complete 
  3. save the new status 
*/
module.exports.updateStatus = (taskId, newStatus) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      result.status = newStatus.status;
      return result.save().then((updatedStatus, error) => {
        if (error) {
          console.log(error);
          return false;
        } else {
          return updatedStatus;
        }
      });
    }
  });
};

// updateStatus without req.body
/* module.exports.updateStatus = (taskId) => {
  return Task.findById(taskId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    }
    result.status = "complete";
    return result.save().then((updatedTask, saveErr) => {
      if (saveErr) {
        console.log(saveErr);
        return saveErr;
      } else {
        return updatedTask;
      }
    });
  });
};
 */
//module.exports allow things to be reusable around the whole file
